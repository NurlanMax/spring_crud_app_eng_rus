package kz.spring.hwone.demo.repo;

import kz.spring.hwone.demo.entities.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public interface TaskRepository extends JpaRepository<Task,Long> {
}
