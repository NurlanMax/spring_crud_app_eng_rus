package kz.spring.hwone.demo.db;

import kz.spring.hwone.demo.entities.Task;

import java.util.ArrayList;

public class DBManager {
    private static ArrayList<Task> tasks = new ArrayList<>();
    private static Long id=5L;
//    static {
//        tasks.add(new Task(1L,"Complete task 7 till the end of the lesson","23.10.2020",true));
//        tasks.add(new Task(2L,"Clean home and buy foods","25.10.2020",true));
//        tasks.add(new Task(3L,"Complete all home tasks at the weekend","28.10.2020",false));
//        tasks.add(new Task(4L,"Learn italian language","01.05.2021",false));
//    }
    public static void addTask(Task task){
        task.setId(id);
        tasks.add(task);
        id++;
    }
    public static ArrayList<Task> getAllTasks() {
        return tasks;
    }
    public static Task getTask(Long id) {
        for(Task tk: tasks) {
            if(tk.getId()==id) return tk;
        }
        return null;
    }
}
