package kz.spring.hwone.demo.controllers;

import kz.spring.hwone.demo.db.DBManager;
import kz.spring.hwone.demo.entities.Task;
import kz.spring.hwone.demo.repo.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
public class HomeController {
    @Autowired
    private TaskRepository taskRepository;

    @GetMapping(value = "/")
    public String homePage(Model model) {
        List<Task> tasks = taskRepository.findAll();
        model.addAttribute("tasks", tasks);
        return "home";
    }

    @PostMapping(value = "/addtask")
    public String addTask(@RequestParam(name = "name") String name,
                          @RequestParam(name = "deadline", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date deadline,
                          @RequestParam(name = "description") String description) {
        Task task = new Task();
        task.setName(name);
        task.setDeadline(deadline);
        task.setDescription(description);
        task.setCompleted(false);
        taskRepository.save(task);
        return "redirect:/addtaskpage";
    }


    @GetMapping(value = "/detail/{id}")
    public String detail(@PathVariable(name = "id") Long id, Model model) {
        Optional<Task> opt = taskRepository.findById(id);
        if (opt.isPresent()) {
            Task task = opt.get();
            model.addAttribute("task", task);
        } else {
            return "redirect:/";
        }
        return "detail";
    }

    @PostMapping(value = "/savetask")
    public String saveTask(@RequestParam(name = "id") Long id,
                           @RequestParam(name = "name") String name,
                           @RequestParam(name = "description") String description,
                           @RequestParam(name = "deadline", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date deadline,
                           @RequestParam(name = "is_completed") boolean isCompleted) {

        Optional<Task> opt = taskRepository.findById(id);
        if (opt.isPresent()) {
            Task task = opt.get();
            task.setName(name);
            task.setDescription(description);
            task.setDeadline(deadline);
            task.setCompleted(isCompleted);
            taskRepository.save(task);
            return "redirect:/detail/" + id;
        }
        return "redirect:/";
    }

    @PostMapping(value = "/deletetask")
    public String deleteTask(@RequestParam(name = "id") Long id) {
        Optional<Task> opt = taskRepository.findById(id);
        if (opt.isPresent()) {
            Task task = opt.get();
            taskRepository.delete(task);
        }
        return "redirect:/";
    }
//    @GetMapping(value = "/detail/{taskId}")
//    public String detail(@PathVariable(name="taskId") Long id, Model model) {
//        Task task=DBManager.getTask(id);
//        model.addAttribute("task", task);
//        return "detail";
//    }
//    @PostMapping(value = "/addtask")
//    public String addItem(@RequestParam(name="name") String name,
//                          @RequestParam(name ="deadline") String deadLine,
//                          @RequestParam(name = "status") boolean isCompleted) {
//        DBManager.addTask(new Task(null, name, deadLine, isCompleted));
//        return "addtask";
//    }

    @GetMapping(value = "/addtaskpage")
    public String addItemPage() {
        return "addtask";
    }
}
